/*
 *  Power BI Visual CLI
 *
 *  Copyright (c) Microsoft Corporation
 *  All rights reserved.
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the ""Software""), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
"use strict";

import "./../style/visual.less";
import powerbi from "powerbi-visuals-api";
import VisualConstructorOptions = powerbi.extensibility.visual.VisualConstructorOptions;
import VisualUpdateOptions = powerbi.extensibility.visual.VisualUpdateOptions;
import IVisual = powerbi.extensibility.visual.IVisual;
import EnumerateVisualObjectInstancesOptions = powerbi.EnumerateVisualObjectInstancesOptions;
import VisualObjectInstance = powerbi.VisualObjectInstance;
import DataView = powerbi.DataView;
import VisualObjectInstanceEnumerationObject = powerbi.VisualObjectInstanceEnumerationObject;
import IVisualHost = powerbi.extensibility.IVisualHost;

import { VisualSettings } from "./settings";

import * as d3 from "d3";
type Selection<T extends d3.BaseType> = d3.Selection<T, any, any, any>;

export class Visual implements IVisual {
  private host: IVisualHost;
  private svg: Selection<SVGElement>;
  private svgWidth;
  private svgHeight;
  private margin: { top: 20; right: 20; bottom: 30; left: 50 };
  private width;
  private height;
  private x;
  private y;

  constructor(options: VisualConstructorOptions) {
    this.svg = d3
      .select(options.element)
      .append("g")
      .attr(
        "transform",
        "translation(" + this.margin.left + "," + this.margin.top + ")"
      )
      .classed("svg", true);

    this.svg
      .append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(this.x))
      .select(".domain")
      .remove();

    this.svg
      .append("g")
      .call(d3.axisLeft(this.y))
      .append("text")
      .attr("fill", "#000")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("price ($)");

    this.svgWidth = this.svg.style("width");
    this.svgHeight = this.svg.style("height");

    this.width = this.svgWidth - this.margin.left - this.margin.right;
    this.height = this.svgHeight - this.margin.top - this.margin.bottom;

    this.x = d3.scaleTime().rangeRound([0, this.width]);
    this.y = d3.scaleLinear().rangeRound([this.height, 0]);
  }

  public update(options: VisualUpdateOptions) {
    let dataViews = options.dataViews;

    var line = d3
      .line()
      .x(function (d) {
        return this.x(dataViews[0].categorical.categories[0]);
      })
      .y(function (d) {
        return this.y(dataViews[0].categorical.values[0]);
      });
    this.x.domain(
      d3.extent(dataViews, function (d):number {
        return +dataViews[0].categorical.categories[0];
      })
    );
    this.y.domain(
      d3.extent(dataViews, function (d): number {
        return +dataViews[0].categorical.values[0];
      })
    );

    this.svg
    .append("path")
    .datum(dataViews)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-linejoin", "round")
    .attr("stroke-linecap", "round")
    .attr("stroke-width", 1.5)
    .attr("d", +line);
  }

  // private static parseSettings(dataView: DataView): VisualSettings {
  //     return <VisualSettings>VisualSettings.parse(dataView);
  // }

  // /**
  //  * This function gets called for each of the objects defined in the capabilities files and allows you to select which of the
  //  * objects and properties you want to expose to the users in the property pane.
  //  *
  //  */
  // public enumerateObjectInstances(options: EnumerateVisualObjectInstancesOptions): VisualObjectInstance[] | VisualObjectInstanceEnumerationObject {
  //     return VisualSettings.enumerateObjectInstances(this.settings || VisualSettings.getDefault(), options);
  // }
}
